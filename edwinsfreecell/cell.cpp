#include "pch.h"
#include "cell.h"
#include "WindowsCards.h"
#include <iostream>


Cell::Cell(int left, int top, int right, int bottom)
{
	mLeft = left;
	mTop = top;
	mRight = right;
	mBottom = bottom;
	mSelected = false;
}
void Cell::Draw(CDC& dc)
{
	CBrush cellBackgroundColor(RGB(128, 128, 128));
	dc.SelectObject(cellBackgroundColor);
	dc.Rectangle(mLeft, mTop, mRight, mBottom);

	if (mCards.size() > 0)
	{
		DrawCard(dc, mLeft + 10, mTop + 2, mCards[mCards.size()-1], mSelected);
	}
}
int Cell::GetTopCard()
{
	return mCards[mCards.size() - 1];
}

void Cell::AddCard(int index)
{
 	mCards.push_back(index);
}
void Cell::RemoveCard()
{
	mCards.pop_back();
}

bool Cell::CanRemoveCard()
{
	return mCards.size() > 0; //returns true or false
}
bool Cell::CanAcceptCard(int index)
{
	return true;
}

bool Cell::IsClicked(int x, int y)
{
	return (x >= mLeft && x <= mRight && y >= mTop && y <= mBottom);

}
void Cell::SetSelected(bool selected)
{
	mSelected = selected;
}


Startcell::Startcell(int left, int top, int right, int bottom)
	:Cell(left, top, right, bottom) {

}

void Startcell::Draw(CDC& dc)
{
	CBrush cellBackgroundColor(RGB(0, 200, 0));
	dc.SelectObject(cellBackgroundColor);

	int startbottom = mBottom + 600;
	dc.Rectangle(mLeft, mTop, mRight, startbottom);

	int height = gCardHeight/3;
	int cardTop = mTop + 15;
	int cardLeft = mLeft + 10;
	for(unsigned int i=0; i<mCards.size(); i++)
	{
		DrawCard(dc, cardLeft, cardTop, mCards[i], mSelected);
		cardTop += height;
	}
}
bool Startcell::CanAcceptCard(int index)
{
	int indexrank = index / 4;
	int indexsuit = index % 4;
	int size = mCards.size();

	if (size == 0)
	{
		return true;
	}

	else if (size > 0)
	{
		int toprank = mCards[mCards.size() - 1] / 4;
		int topsuit = mCards[mCards.size() - 1] % 4;
		if ((indexsuit == 0 || indexsuit == 3) && (topsuit == 1 || topsuit == 2) // for black cards placed on red
			&& (indexrank == toprank - 1) )
		{
			return true;
		}
		else if ((indexsuit == 1 || indexsuit == 2) && (topsuit == 0 || topsuit == 3) // for red cards placed on black
			&& (indexrank == toprank - 1) )
		{
			return true;
		}
		else
			return false;
	}
	else
		return false;
	
}
Endcell::Endcell(int left, int top, int right, int bottom)
	:Cell(left, top, right, bottom) {
					
}

bool Endcell::CanAcceptCard(int index)
{
	int indexrank = index / 4;
	int indexsuit = index % 4;
	int size = mCards.size();
	if (size == 0)
	{
		if (indexrank == 0 )
		{
			return true;
		}
	}
	if (size > 0)
	{
		int toprank = mCards[mCards.size() - 1] / 4;
		int topsuit = mCards[mCards.size() - 1] % 4;
		if (indexrank == toprank + 1 && indexsuit == topsuit)
		{
			return true;
		}
		else
		{
			return false;
		}

	}
	else
	{
		return false;
	}

}

bool Endcell::CanRemoveCard()
{
	return false;
}

Freecell::Freecell(int left, int top, int right, int bottom)
	:Cell(left, top, right, bottom) {

}

bool Freecell::CanAcceptCard(int index)
{
	int size = mCards.size();
	if (size == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}