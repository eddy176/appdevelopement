
// edwinsfreecellDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "edwinsfreecell.h"
#include "cell.h"
#include "edwinsfreecellDlg.h"
#include "afxdialogex.h"
#include "WindowsCards.h"
#include <iostream>
#include <algorithm>
#include <random>
#include <chrono>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()

// CedwinsfreecellDlg dialog

CedwinsfreecellDlg::CedwinsfreecellDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_EDWINSFREECELL_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CedwinsfreecellDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CedwinsfreecellDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDCANCEL, &CedwinsfreecellDlg::OnBnClickedCancel)
//	ON_WM_SIZE()
ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()

// CedwinsfreecellDlg message handlers

BOOL CedwinsfreecellDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	bool ok = InitializeCards();
	int left = 10;
	int top = 10;
	int right = 100;
	int bottom = 110;

	int endleft = 460;
	int endright = 550;

	int startleft = 30;
	int startright = 125;
	int starttop = 130;
	int startbottom = 710;

	//draw cells
	for (int i = 0; i < 16; i++)
	{
		if (i < 4)
		{
			mCells[i] = new Freecell(left, top, right, bottom);
			left += 100;
			right += 100;
		}

		if (i > 3 && i < 8)
		{
			mCells[i] = new Endcell(endleft, top, endright, bottom);
			endleft += 100;
			endright += 100;
		}
		
		if (i > 7 && i < 16)
		{
			mCells[i] = new Startcell(startleft, starttop, startright, startbottom);
			startleft += 100;
			startright += 100;
		}
	}
	mfirstSelected = -1;

	//shuffle list from 0-51 to get random indexes for cards
	std::vector<int> randints;
	for (int j = 0; j < 52; j++)
	{
		randints.push_back(j);
	}

	srand((unsigned int)time(0));
	for (int j=0; j<52; j++)
	{
		int k = rand() % 52;
		int temp = randints[j];
		randints[j] = randints[k];
		randints[k] = temp;
	}

	//initialize start cards
	for (int i = 8; i < 16; i++)
	{
			
		if (i == 8)
		{
			for (int j = 0; j < 7; j++)
			{
				mCells[i]->AddCard(randints[j]);
			}
				
		}
		else if (i == 9)
		{
			for (int j = 7; j < 14; j++)
			{
				mCells[i]->AddCard(randints[j]);
			}
		}
		else if (i == 10)
		{
			for (int j = 14; j < 21; j++)
			{
				mCells[i]->AddCard(randints[j]);
			}
		}
		else if (i == 11)
		{
			for (int j = 21; j < 28; j++)
			{
				mCells[i]->AddCard(randints[j]);
			}
		}
		else if (i == 12)
		{
			for (int j = 28; j < 34; j++)
			{
				mCells[i]->AddCard(randints[j]);
			}
		}
		else if (i == 13)
		{
			for (int j = 34; j < 40; j++)
			{
				mCells[i]->AddCard(randints[j]);
			}
		}
		else if (i == 14)
		{
			for (int j = 40; j < 46; j++)
			{
				mCells[i]->AddCard(randints[j]);
			}
		}
		else if ( i == 15)
		{
			for (int j = 46; j < 52; j++)
			{
				mCells[i]->AddCard(randints[j]);
			}
		}
		
	}
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CedwinsfreecellDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CedwinsfreecellDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CPaintDC dc(this); // device context for painting
		for (int i = 0; i <16; i++)
		{
			mCells[i]->Draw(dc);

		}
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CedwinsfreecellDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CedwinsfreecellDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here

	CDialogEx::OnCancel();
}

void CedwinsfreecellDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	//draw cells
	CRect rect;
	GetClientRect(&rect);
	int picked = -1;
	
	for (int i = 0; i < 16; i++)
	{
		if (mCells[i]->IsClicked(point.x, point.y) == true)
		{
			picked = i;
		}
	}
	if (picked == -1)
		return;

	if (mfirstSelected == -1) // first of two clicks
	{
		if (mCells[picked]->CanRemoveCard())
		{
			mfirstSelected = picked; //this is the selected card of first click
			mCells[picked]->SetSelected(true);
		}
	}
	else if (mfirstSelected == picked)
	{
		mCells[mfirstSelected]->SetSelected(false);
		mfirstSelected = -1;

	}
	else // second of two clicks. mfirstSelected is first click picked is second click
	{
		int topcardfromfirstclick = mCells[mfirstSelected]->GetTopCard();

		if (mCells[picked]->CanAcceptCard(topcardfromfirstclick))
		{
			//remove card from mfirstselected and add it to picked and reset mfirstselected
			mCells[mfirstSelected]->RemoveCard();
			mCells[picked]->AddCard(topcardfromfirstclick);
			mCells[mfirstSelected]->SetSelected(false);
			mfirstSelected = -1;
			
		}
	}
	Invalidate(); //calls paint method
	CDialogEx::OnLButtonDown(nFlags, point);
}
