#pragma once
#include <vector>
class Cell
{
public:

	Cell(int left, int top, int right, int bottom);
	virtual void Draw(CDC& dc);

	void AddCard(int index);
	void RemoveCard();

	virtual bool CanRemoveCard();
	virtual bool CanAcceptCard(int index);

	bool IsClicked(int x, int y);
	void SetSelected(bool selected);
	int GetTopCard();

protected:
	std::vector<int> mCards;
	int mLeft, mTop, mRight, mBottom;
	bool mSelected;
};

class Startcell : public Cell
{
public:
	Startcell(int left, int top, int right, int bottom);
	virtual void Draw(CDC& dc);
	virtual bool CanAcceptCard(int index);
};

class Freecell : public Cell
{
public:
	Freecell(int left, int top, int right, int bottom);
	virtual bool CanAcceptCard(int index);

};

class Endcell : public Cell
{
public:
	Endcell(int left, int top, int right, int bottom);
	//virtual void Draw(CDC& dc);
	virtual bool CanAcceptCard(int index);
	virtual bool CanRemoveCard();

};
//click dlg freecell rc click dialog find messages find wml button down on l button down then it is added in frecelldlg h and cpp
//implement double click system like towers of hanoi. need to check if clicked cells can remove and or accept cards
//random function
//get top card function

