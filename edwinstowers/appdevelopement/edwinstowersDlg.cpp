
// edwinstowersDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "edwinstowers.h"
#include "edwinstowersDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//constants for creating towers
const double MARGIN = 10.0;
const double BASE_WIDTH = 20.0;
const double BASE_HEIGHT = 10.0;
const double POLE_WIDTH = 4.0;
const double POLE_HEIGHT = 60.0;
const int NUM_DISKS = 4;
const double DISK_WIDTH_DECREASE = 2.0;
const double DISK_HEIGHT = 8.0;


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CedwinstowersDlg dialog



CedwinstowersDlg::CedwinstowersDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_EDWINSTOWERS_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CedwinstowersDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CedwinstowersDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_SIZE()
	ON_WM_LBUTTONDOWN()
END_MESSAGE_MAP()


// CedwinstowersDlg message handlers

BOOL CedwinstowersDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization 
	double width = BASE_WIDTH - DISK_WIDTH_DECREASE;
	for (int i = 0; i < NUM_DISKS; i++)
	{
		mDisks[0].push_back(width);
		width -= DISK_WIDTH_DECREASE;
	}
	mSelectedTower = -1;

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CedwinstowersDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CedwinstowersDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		//device context needed to paint
		CPaintDC dc(this);
		CRect rect;
		GetClientRect(&rect);
		CBrush blueBrush(RGB(0, 0, 255));
		CBrush redBrush(RGB(255, 0, 0));
		
		// draw 3 towers
		for (int i = 0; i < 3; i++)
		{
			//drawing base of tower
			double left = MARGIN + i * (MARGIN + BASE_WIDTH);
			double right = left + BASE_WIDTH;
			double bottom = MARGIN;
			double top = bottom + BASE_HEIGHT;

			int l = (int)(left * rect.Width() / 100);
			int b = (int)(rect.Height() - bottom * rect.Height() / 100);
			int r = (int)(right * rect.Width() / 100);
			int t = (int)(rect.Height() - top * rect.Height() / 100);
			dc.SelectObject(blueBrush);
			dc.Rectangle(l, b, r, t);

			//draw pole of the tower
			double middle = left + BASE_WIDTH / 2;
			left = middle - POLE_WIDTH / 2;
			right = left + POLE_WIDTH;
			bottom = MARGIN + BASE_HEIGHT;
			top = bottom + POLE_HEIGHT;

			//converts to pixel coords
			l = (int)(left * rect.Width() / 100);
			b = (int)(rect.Height() - bottom * rect.Height() / 100);
			r = (int)(right * rect.Width() / 100);
			t = (int)(rect.Height() - top * rect.Height() / 100);
			dc.Rectangle(l, b, r, t);
		
			//draws disks of tower 
			dc.SelectObject(redBrush);
			for (size_t d = 0; d < mDisks[i].size(); d++)
			{
				double width = mDisks[i][d];
				left = middle - width / 2;
				right = left + width;
				bottom = MARGIN + BASE_HEIGHT + d * DISK_HEIGHT;
				top = bottom + DISK_HEIGHT;

				l = (int)(left * rect.Width() / 100);
				b = (int)(rect.Height() - bottom * rect.Height() / 100);
				r = (int)(right * rect.Width() / 100);
				t = (int)(rect.Height() - top * rect.Height() / 100);

				dc.Rectangle(l, b, r, t + 2);

			}


		}
			CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CedwinstowersDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//need class variable to keep track of frist and second click
void CedwinstowersDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default

	// find out which tower variable point is inside of:
	CRect rect;
	GetClientRect(&rect);
	int picked = -1;
	for (int i = 0; i < 3; i++)
	{
		// Check the base of the tower
		double left = MARGIN + i * (BASE_WIDTH + MARGIN);
		double right = left + BASE_WIDTH;
		double bottom = MARGIN;
		double top = bottom + BASE_HEIGHT;

		int l = (int)(left * rect.Width() / 100);
		int b = (int)(rect.Height() - bottom * rect.Height() / 100);
		int r = (int)(right * rect.Width() / 100);
		int t = (int)(rect.Height() - top * rect.Height() / 100);
		if (point.x >= l && point.x <= r && point.y <= b && point.y >= t)
			picked = i;

		// Check the pole of the tower:
		double middle = left + BASE_WIDTH / 2;
		left = middle - POLE_WIDTH / 2;
		right = left + POLE_WIDTH;
		bottom = MARGIN + BASE_HEIGHT;
		top = bottom + POLE_HEIGHT;

		l = (int)(left * rect.Width() / 100);
		b = (int)(rect.Height() - bottom * rect.Height() / 100);
		r = (int)(right * rect.Width() / 100);
		t = (int)(rect.Height() - top * rect.Height() / 100);
		if (point.x >= l && point.x <= r && point.y <= b && point.y >= t)
			picked = i;

		// Check the disks of this tower:
		for (size_t d = 0; d < mDisks[i].size(); d++)
		{
			double width = mDisks[i][d];
			left = middle - width / 2;
			right = left + width;
			bottom = MARGIN + BASE_HEIGHT + d * DISK_HEIGHT;
			top = bottom + DISK_HEIGHT;

			l = (int)(left * rect.Width() / 100);
			b = (int)(rect.Height() - bottom * rect.Height() / 100);
			r = (int)(right * rect.Width() / 100);
			t = (int)(rect.Height() - top * rect.Height() / 100);
			if (point.x >= l && point.x <= r && point.y <= b && point.y >= t + 2)
				picked = i;
		}
	}

	if (picked == -1)
	{
		return;
	}

	if (mSelectedTower == -1) // This is a first click, of a two click move
	{
		if (mDisks[picked].size() > 0)
		{
			mSelectedTower = picked;
		}
	}
	else // This is the second click, of a two click move
	{
		// make sure it's legal to move a disk from mSelectedTower to picked tower:
		if (mDisks[picked].size() == 0 ||
			mDisks[picked][mDisks[picked].size() - 1] > mDisks[mSelectedTower][mDisks[mSelectedTower].size() - 1])
		{
			// Do the move!
			mDisks[picked].push_back(mDisks[mSelectedTower][mDisks[mSelectedTower].size() - 1]);
			mDisks[mSelectedTower].pop_back();
			mSelectedTower = -1;
		}
	}
	Invalidate();
	CDialogEx::OnLButtonDown(nFlags, point);
}



//resizes
void CedwinstowersDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	// TODO: Add your message handler code here
	Invalidate();
}


